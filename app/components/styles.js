import { StyleSheet } from 'react-native'

import {
    defaultStyles,
    primaryColor,
    whiteColor,
    primarylighter2Color,
    primarylighter3Color
} from '../styles/defaultStyles'

let selfStyle= StyleSheet.create({
    container:{
        backgroundColor: primaryColor,
        color: whiteColor,
        flex:1,
        flexDirection: 'column',

        justifyContent: 'space-between',
        // alignItems: 'center',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        padding:10,
        position:'absolute',
        top:0,
        bottom:0,
        width: '100%'
    },
    tabBar:{
        backgroundColor: whiteColor,
        color: primaryColor,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 30,
        height: 65,
        width: '100%',
        padding:15,
        // margin: 20,
    },
    tabBarMenu:{
        backgroundColor: whiteColor,
        color: primaryColor,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        borderRadius: 25,
        height: 50,
        padding:15,
        // width: '100%'
    },
    tabBarMenuActive : {
        backgroundColor: primarylighter3Color,
    }
})

export const container = StyleSheet.flatten([
    defaultStyles.view, selfStyle.container
])

export const tabBar = StyleSheet.flatten([
    defaultStyles.view, selfStyle.tabBar
])

export const tabBarMenu = StyleSheet.flatten([
    defaultStyles.view, selfStyle.tabBarMenu
])

export const tabBarMenuActive = selfStyle.tabBarMenuActive
