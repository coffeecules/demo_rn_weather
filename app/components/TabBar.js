/* /src/components/TabBar.js */

import React from "react";
import { View, Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Animated, Dimensions
 } from "react-native";
 import {Spring} from 'react-spring/renderprops'
import { animated } from "react-spring/renderprops-universal";
import RadarSubTaBar from "../modules/radar/subTabBar";
import ForecastSubTaBar from "../modules/forecast/subTabBar";
import {container,
   tabBar,
   tabBarMenu,
   tabBarMenuActive} from "./styles";


export default class TabBar extends React.Component{
    state = {
      containerTop: new Animated.Value(Dimensions.get("window").height*60/100),
      menuWidth: [  new Animated.Value(100),  new Animated.Value(50),  new Animated.Value(50)],
    };

    animatedMapperFunc = (index, activeRouteIndex)=>{
        const {menuWidth} = this.state;
        if(index === activeRouteIndex){
            return Animated.timing(menuWidth[index], {
                toValue: 100,
                duration: 500
              })
        }
        else{
            return Animated.timing(menuWidth[index], {
                toValue: 50,
                duration: 500
              })
        }

    }

    componentDidUpdate(){
    const {
        navigation
      } = this.props;
      const {containerTop, menuWidth} = this.state;

      const { routes, index: activeRouteIndex } = navigation.state;

      if(activeRouteIndex===0){
        Animated.parallel([
            Animated.spring(containerTop, {
                toValue: Dimensions.get("window").height*60/100,
                duration: 500
              }),
              Animated.timing(menuWidth[0], {
                  toValue: 100,
                  duration: 500
              }),
              Animated.timing(menuWidth[1], {
                  toValue: 50,
                  duration: 500
              }),
              Animated.timing(menuWidth[2], {
                  toValue: 50,
                  duration: 500
              })
            // menuWidth.map((value, index)=>this.animatedMapperFunc(index, activeRouteIndex))
        ]).start();
      }
      else if(activeRouteIndex===1){
        Animated.parallel([
            Animated.spring(containerTop, {
                toValue: Dimensions.get("window").height*80/100,
                duration: 500
            }),
            Animated.timing(menuWidth[0], {
                toValue: 50,
                duration: 500
            }),
            Animated.timing(menuWidth[1], {
                toValue: 100,
                duration: 500
            }),
            Animated.timing(menuWidth[2], {
                toValue: 50,
                duration: 500
            })
        ]).start();
      }
      else if(activeRouteIndex===2){
        Animated.parallel([
            Animated.spring(containerTop, {
                toValue: 0,
                duration: 500
            }),
            Animated.timing(menuWidth[0], {
                toValue: 50,
                duration: 500
            }),
            Animated.timing(menuWidth[1], {
                toValue: 50,
                duration: 500
            }),
            Animated.timing(menuWidth[2], {
                toValue: 100,
                duration: 500
            })
        ]).start();
      }
    }

    render(){
        const {
            renderIcon,
            getLabelText,
            activeTintColor,
            inactiveTintColor,
            onTabPress,
            onTabLongPress,
            getAccessibilityLabel,
            navigation
          } = this.props;

        const {
            containerTop,
            menuWidth
        } = this.state;

        const { routes, index: activeRouteIndex } = navigation.state;

        const containerAnimatedStyles = {
            top: containerTop,
        }

        return (
            <Animated.View style={[container, containerAnimatedStyles]}>
                {activeRouteIndex === 0 && <ForecastSubTaBar></ForecastSubTaBar>}
                {activeRouteIndex === 1 && <RadarSubTaBar></RadarSubTaBar>}
                {activeRouteIndex === 2 && <View></View>}
                <View style={tabBar}>
                    {routes.map((route, routeIndex) => {
                        const isRouteActive = routeIndex === activeRouteIndex;
                        const tintColor = isRouteActive ? activeTintColor : inactiveTintColor;

                        return (<TouchableWithoutFeedback
                            key={routeIndex}
                            style={{
                                borderColor: 'white',
                                borderWidth: 2,
                            }}
                            onPress={() => {onTabPress({ route })}}
                            onLongPress={() => {onTabLongPress({ route })}}
                            accessibilityLabel={getAccessibilityLabel({ route })}
                        >
                            <Animated.View style={[tabBarMenu, isRouteActive? tabBarMenuActive:{}, {width:menuWidth[routeIndex]}]}>
                                {renderIcon({ route, focused: isRouteActive, tintColor })}
                                {isRouteActive && <Text style={{marginLeft:5, flexWrap:'wrap'}}>{getLabelText({ route })}</Text>}
                            </Animated.View>
                        </TouchableWithoutFeedback>);
                    })}
                </View>
            </Animated.View>
        );
    }
}
