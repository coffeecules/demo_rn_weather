import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import forecast from "../modules/forecast";
import TabNavigator from "./TabNavigator";

// const mainNavigator = createStackNavigator({
//     forecast: {screen: forecast}
// })

// const App = createAppContainer(mainNavigator)

// export default App
export default createAppContainer(TabNavigator);