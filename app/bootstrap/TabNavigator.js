import React from "react";
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Ionicons } from '@expo/vector-icons';
import forecast from "../modules/forecast";
import radar from "../modules/radar";
import settings from "../modules/settings";
import TabBar from "../components/TabBar";
const activeColor = "#4775f2";
const inactiveColor = "#b8bece";

const forecastStack = createStackNavigator({
    Forecast : forecast
})

forecastStack.navigationOptions = {
    tabBarLabel: "Forecast",
    tabBarIcon: ({ focused }) => (
      <Ionicons
        name="md-calendar"
        size={26}
        color={focused ? activeColor : inactiveColor}
      />
    )
  };

const radarStack = createStackNavigator({
    Settings : radar,
})

radarStack.navigationOptions = {
    tabBarLabel: "Radar",
    tabBarIcon: ({ focused }) => (
      <Ionicons
        name="md-globe"
        size={26}
        color={focused ? activeColor : inactiveColor}
      />
    )
  };

const settingsStack = createStackNavigator({
    Settings : settings,
})

settingsStack.navigationOptions = {
    tabBarLabel: "Settings",
    tabBarIcon: ({ focused }) => (
      <Ionicons
        name="md-settings"
        size={26}
        color={focused ? activeColor : inactiveColor}
      />
    )
  };

export default TabNavigator = createBottomTabNavigator({
    forecastStack,
    radarStack,
    settingsStack
},

{
  tabBarComponent: TabBar,
})
