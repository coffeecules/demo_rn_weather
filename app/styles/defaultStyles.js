import { StyleSheet } from 'react-native'

export const primarylighter3Color = '#E4E2FD'
export const primarylighter2Color = '#AAA5F4'
export const primaryColor = '#0303D3'
export const primaryDarker2Color = '#837CF2'
export const primaryDarker3Color = '#4944D5'
export const whiteColor = 'white'

export const defaultStyles = StyleSheet.create({
    view: {
        backgroundColor: primaryColor,
        color: 'white'
    },
    text: {
        color: 'white'
    },
  });