import {View, Text, ScrollView}  from 'react-native'
import React from 'react'
import { Ionicons } from '@expo/vector-icons';
import LottieView from 'lottie-react-native';
import {container} from './styles'
import {
    whiteColor
} from '../../styles/defaultStyles'

export default class ForecastSubTaBar extends React.Component{
    static navigationOptions = {
      header: null
    };

    render(){
        return(
        <ScrollView
          horizontal={true}
          style={{ width:'100%' }}
          showsHorizontalScrollIndicator={false}
        >
          <View style={{
            justifyContent: 'space-between',
            alignItems: 'center',
            // width: 200,
          }}>
            <LottieView source={require('../../../assets/7707-fog-white.json')} autoPlay loop />
            <Text style={{color:whiteColor, fontSize:26}}>8:15</Text>
            <Text style={{color:whiteColor, fontSize:14}}>14</Text>
          </View>
        </ScrollView>
        )
    }
}
