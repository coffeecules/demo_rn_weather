import {View, Text, SafeAreaView}  from 'react-native'
import React from 'react'
import {container} from './styles'

export default class radar extends React.Component{
    static navigationOptions = {
      header: null
    };

    render(){
        return(<SafeAreaView>
          <View style={container}>
            <Text>radar screen</Text>
        </View>
      </SafeAreaView>)
    }
}
