import {View, Text}  from 'react-native'
import React from 'react'
import { Ionicons } from '@expo/vector-icons';
import {container} from './styles'
import {
    whiteColor
} from '../../styles/defaultStyles'

export default class RadarSubTaBar extends React.Component{
    static navigationOptions = {
      header: null
    };

    render(){
        return(<View style={{
          flexDirection:'row',
          paddingLeft: 10,
          paddingRight: 10,
          paddingTop: 'auto',
          paddingBottom: 'auto',
          justifyContent:'space-between',
          alignItems: 'center',
          height:75
        }}>
          <Ionicons
            name="md-pause"
            size={26}
            color={whiteColor}
          />
          <Text style={{color:whiteColor, fontSize:26}}>8:15</Text>
          <View></View>
        </View>)
    }
}
